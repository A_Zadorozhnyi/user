package redactor;

/**
 * Created by Alex_B on 27.03.14.
 */

public class User {
    String name;
    String first;
    String second;
    String third;


    User(String nAbonent, String fNumber, String sNumber, String tNumber) {
        name = nAbonent;
        first = fNumber;
        second = sNumber;
        third = tNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getThird() {
        return third;
    }

    public void setThird(String third) {
        this.third = third;
    }
}


