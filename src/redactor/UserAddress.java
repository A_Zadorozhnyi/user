package redactor;

public class UserAddress {
    String zip;
    String street;

    public UserAddress(String zip, String street) {
        this.zip = zip;
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
