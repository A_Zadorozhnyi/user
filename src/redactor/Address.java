package redactor;

/**
 * Created by Alex_B on 27.03.14.
 */
public class Address {

    UserAddress address;

    public Address(String zip, String street) {
        this.address = new UserAddress(zip, street);
    }

    public UserAddress getAddress() {
        return address;
    }

    public void setAddress(UserAddress address) {
        this.address = address;
    }
}

