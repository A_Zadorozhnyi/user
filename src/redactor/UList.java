package redactor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex_B on 26.03.14.
 */
public class UList {
    List<User> oneList = new ArrayList<User>();
   UList() {
       oneList.add(new User("Valera", "+380", "099", "34-23-124"));
       oneList.add(new User("Ira", "+380", "093", "76-54-321"));
       oneList.add(new User("Tema", "+380", "096", "95-37-343"));
       oneList.add(new User("Alex", "+380", "066", "34-24-123"));
   }

    public List<User> getOneList() {
        return oneList;
    }

    public void setOneList(List<User> oneList) {
        this.oneList = oneList;
    }
    public void setElemOneList (int i, String nAbonent, String fNumber, String sNumber, String tNumber){
        this.oneList.set(i, new User(nAbonent, fNumber, sNumber, tNumber));
    }
}
