package redactor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "Servlet", urlPatterns = "/ht")
public class MainServlet extends HttpServlet {
    HttpSession htksession;
    UList users;


    String[][] userAddress = {{"Engelsa street", "18002"}, {"Shevchenka street", "18000"}, {"Smilyanska avenu", "18001"}};


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        htksession = request.getSession(true);
        request.setCharacterEncoding("UTF-8");

        List<Address> users2 = new LinkedList<Address>();
        for (String[] strin : userAddress) {
            String zip = strin[1];
            String street = strin[0];
            users2.add(createAddress(zip, street));
        }
        request.setAttribute("Address", users2);

        if (htksession.getAttribute("users") == null) {
            if (users == null) {
                users = new UList();
            }
            htksession.setAttribute("users", users.getOneList());
        }
        if ("set".equals(request.getParameter("action"))) {
            if (users == null) {
                users = new UList();
            }
            users.setOneList((List) htksession.getAttribute("users"));
            int number = Integer.parseInt(request.getParameter("number")) - 1;
            users.setElemOneList(number, request.getParameter("name"), request.getParameter("first"), request.getParameter("second"), request.getParameter("third"));
            htksession.setAttribute("users", users.getOneList());
            request.getRequestDispatcher("/main.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        List<Address> users2 = new LinkedList<Address>();
        for (String[] strin : userAddress) {
            String zip = strin[1];
            String street = strin[0];
            users2.add(createAddress(zip, street));
        }

        request.setAttribute("Address", users2);


        htksession = request.getSession(true);
        if (htksession.getAttribute("users") == null) {
            if (users == null) {
                users = new UList();
            }
            htksession.setAttribute("users", users.getOneList());
        }

        if (request.getParameter("action") == null || "view".equals(request.getParameter("action"))) {
            request.getRequestDispatcher("/main.jsp").forward(request, response);
        }
        if ("edit".equals(request.getParameter("action"))) {
            int number = Integer.parseInt(request.getParameter("number"));
            request.setAttribute("user", users.getOneList().get(number - 1));
            request.setAttribute("number", Integer.toString(number));
            request.getRequestDispatcher("/edit.jsp").forward(request, response);


        }

    }

    public Address createAddress(String zip, String street) {
        return new Address(zip, street);
    }

}























