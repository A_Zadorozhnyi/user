package Car;

import Car.classes.Car;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Alex_B on 06.04.14.
 */

@WebServlet("/car")
public class CarServlet extends HttpServlet {

    private final String CAR = "car";
    String left = "Turn left";
    String right = "Turn right";
    String error = "Error";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String left = request.getParameter("left");
        String right = request.getParameter("right");
        String error = request.getParameter("error");
        String turn = request.getParameter("turn");
        HttpSession session = request.getSession();

        Car car = (Car) session.getAttribute(CAR);
/*
        if (turn.equals("l")) {
            car.setTurn(left);

        } else if (turn.equals("r")) {
            car.setTurn(right);

        } else {
            car.setTurn(error);

        }
        */
        car = new Car();
        car.setTurn(turn);

        session.setAttribute(CAR, car);
//        response.sendRedirect("/");
        request.getServletContext().getRequestDispatcher("/car.jsp").forward(request, response);

        //      List objectList = dataLoader.getList(); // загружаете данные из базы
        //      request.setAttribute("objectList", objectList);
        //      request.getRequestDispatcher("list.jsp").forward(request, response);
    }
}


