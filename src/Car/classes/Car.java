package Car.classes;

/**
 * Created by Alex_B on 06.04.14.
 */

public class Car {
    String turn;
    String left = "Turn left";
    String right = "Turn right";

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

}
