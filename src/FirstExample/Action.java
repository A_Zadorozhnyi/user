package FirstExample;

/**
 * Created by Alex_B on 19.04.14.
 */
public interface Action {
   public String turnLeft();
   public String turnRight();
   public String straight();
}
