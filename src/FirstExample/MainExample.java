package FirstExample;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Alex_B on 11.04.14.
 */
@WebServlet("/fff")
public class MainExample extends HttpServlet {
    HttpSession session;
    int nleft = 0;
    int nright = 0;
    int nstraight = 0;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        session = request.getSession();

        Action action = (Action) session.getAttribute("action");
        String version = (String) request.getParameter("action");

        String result;

        if (version.equals("turnLeft")) {
            result = action.turnLeft();
            nleft = nleft + 1;
            nright = nright;
            nstraight = nstraight;
        } else if (version.equals("turnRight")) {
            result = action.turnRight();
            nleft = nleft;
            nright = nright + 1;
            nstraight = nstraight;
        } else if (version.equals("straight")) {
            result = action.straight();
            nleft = nleft;
            nright = nright;
            nstraight = nstraight + 1;
        } else {
            result = "Error, make your choice";

        }

        session.setAttribute("action", action);
        request.setAttribute("result", result);
        request.setAttribute("nleft", nleft);
        request.setAttribute("nright", nright);
        request.setAttribute("nstraight", nstraight);


        getServletConfig().getServletContext().getRequestDispatcher("/secondChoise.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int nleft = 0;
        int nright = 0;
        int nstraight = 0;
        getServletConfig().getServletContext().getRequestDispatcher("/secondChoise.jsp").forward(request, response);
    }
}

