package FirstExample;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Alex_B on 17.04.14.
 */

@WebServlet("/fffCar")
public class ChoiseCar extends HttpServlet {

    HttpSession session;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        session = request.getSession();
        String allCar = request.getParameter("allCar");


        if (allCar.equals("solarCar")) {

            session.setAttribute("action", new SolarCar());

        } else if ("gasCar".equals(allCar)) {

            session.setAttribute("action", new GasCar());

        } else if ("boat".equals(allCar)) {
            session.setAttribute("action", new Boat());
        }
        response.sendRedirect("/fff");
//        getServletConfig().getServletContext().getRequestDispatcher("/fff").forward(request, response);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        getServletConfig().getServletContext().getRequestDispatcher("/choiceCar.jsp").forward(request, response);
    }
}



