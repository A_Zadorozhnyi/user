package FirstExample;

/**
 * Created by Alex_B on 19.04.14.
 */
public class SolarCar implements Action {

    String turnLeft = "You turn left";
    String turnRight = "You turn right";
    String straight = "You go straight";

    public String getTurnLeft() {
        return turnLeft;
    }

    public void setTurnLeft(String turnLeft) {
        this.turnLeft = turnLeft;
    }

    public String getTurnRight() {
        return turnRight;
    }

    public void setTurnRight(String turnRight) {
        this.turnRight = turnRight;
    }

    public String getStraight() {
        return straight;
    }

    public void setStraight(String straight) {
        this.straight = straight;
    }


    @Override
    public String turnLeft() {
        return turnLeft;
    }

    @Override
    public String turnRight() {
        return turnRight;
    }

    @Override
    public String straight() {
        return straight;
    }
}
