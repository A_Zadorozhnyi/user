<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit</title>
</head>
<body>
<center>
    Edit Form!
    <table align="center" style="width: 400px;" height="80" bgcolor="#ffffcc" cellpadding="10" border="1">
        <form action="/ht" method=post>
            <input type=hidden name=action value="set">
            <input type=hidden name=number value="${number}"></br>

            <tr>
                <td>
                    Имя:
                </td>
                <td>
                    <input type=text name=name maxlength=20 value='${user.name}'>
                </td>
            </tr>
            <tr>
                <td>
                    Код страны:
                </td>
                <td>
                    <input type=text name=first maxlength=20 value='${user.first}'>
                </td>
            </tr>
            <tr>
                <td>
                    Код оператора:
                </td>
                <td>
                    <input type=text name=second maxlength=20 value='${user.second}'>
                </td>
            </tr>
            <tr>
                <td>
                    Номер:
                </td>
                <td>
                    <input type=text name=third maxlength=20 value='${user.third}'>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <input type=submit value='Save'>

                    <form action="/ht" method="get">
                        <input type=hidden name=action value="view">
                        <input type=submit value='Ignore'>
                    </form>
                </td>
            </tr>
        </form>
    </table>
</center>
</body>
</html>
